module.exports = {
    app      : {
        lang      : 'ru',
        stylus    : {
            theme_color : '#3E50B4'
        },
        GA        : false, // Google Analytics site's ID
        package   : 'ключ перезаписывается значениями из package.json marmelad-сборщика',
        settings  : 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
        storage   : 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
        buildTime : '',
        controls : [
            'default',
            'brand',
            'success',
            'info',
            'warning',
            'danger'
        ]
    },

    lotItem: [
        {
            'lotSticker_discount': 'discount',
            'img': 'product-img.jpg',
            'name': 'Говядина тушёная «Кронидов», высший сорт',
            'weight': 'Вес 325 г',
            'currentPrice': '490 р.',
            'oldPrice': '560 р.'
        },
        {
            'lotSticker_hit': 'hit',
            'img': 'product-img2.jpg',
            'name': 'Специи для глинтвейна Organic food',
            'weight': 'Вес 60 г',
            'currentPrice': '98 р.',
            'oldPrice': ''
        },
        {
            'img': 'product-img3.jpg',
            'name': 'Рассольник с потрошками «Каша из топора»',
            'weight': 'Вес 220 г',
            'currentPrice': '560 р.',
            'oldPrice': ''
        },
        {
            'lotSticker_new': 'new',
            'img': 'product-img4.jpg',
            'name': 'Основа для рыбных блюд «Здоровая еда» в ПЭТ-банке',
            'weight': 'Вес 150 г',
            'currentPrice': '200 р.',
            'oldPrice': ''
        },
        {
            'lotSticker_new': '',
            'img': 'product-img6.jpg',
            'name': 'Суп Полевой «Гала-Гала»',
            'weight': 'Вес 60 г',
            'currentPrice': '98 р.',
            'oldPrice': ''
        }
    ],

    advantages: [
        {
            'icon': 'advan-icon.png',
            'title': 'Натуральные ингредиенты',
            'desc': 'Наша продукция не содержит пищевых добавок и вредных для здоровья продуктов.'
        },
        {
            'icon': 'advan-icon2.png',
            'title': 'Быстрая доставка по Москве и области',
            'desc': 'Доставим ваш заказ в любую точку города за считанные минуты.'
        },
        {
            'icon': 'advan-icon3.png',
            'title': 'Еда как от шеф-повара',
            'desc': 'Блюда получаются вкусными, и питательными, как в лучших ресторанах.'
        }
    ],

    ourBrands: [
        {
            'img': 'brends-img.jpg'
        },
        {
            'img': 'brends-img2.jpg'
        },
        {
            'img': 'brends-img3.jpg'
        },
        {
            'img': 'brends-img4.jpg'
        },
        {
            'img': 'brends-img5.jpg'
        },
        {
            'img': 'brends-img6.jpg'
        },
        {
            'img': 'brends-img7.jpg'
        },
        {
            'img': 'brends-img4.jpg'
        }
    ],

    contentPics : [
        {
            "pic" : "img/content-pic1.png",
            "title" : "Спецодежда и обувь (h3)",
            "body" : "При разработке новых моделей, конструкторский отдел Компании опирается на профессиональный опыт спецподразделений, учитывает потребности и специфику различных служб и родов войск."
        },
        {
            "pic" : "img/content-pic2.png",
            "title" : "Спецодежда и обувь 222",
            "body" : "При разработке новых моделей, конструкторский отдел Компании опирается."
        },
        {
            "pic" : "img/content-pic3.png",
            "title" : "Спецодежда и обувь 333",
            "body" : "Конструкторский отдел Компании опирается"
        }
    ],

    contentSlider : [
        {
            "pic" : "img/cont-slid1.png",
            "text" : "Туризм и активный отдых"
        },
        {
            "pic" : "img/cont-slid2.png",
            "text" : "Тактика и камуфляж"
        },
        {
            "pic" : "img/cont-slid3.png",
            "text" : "Рыбалка и охота"
        },
        {
            "pic" : "img/cont-slid4.png",
            "text" : "Охрана и госслужба"
        },
        {
            "pic" : "img/cont-slid5.png",
            "text" : "Сплавы на байдарках"
        }
    ],

    crumbs: {
        catalogPage : [
            {
                title : "Главная",
                url  : "#"
            },
            {
                title : "ПЕРВЫЕ БЛЮДА",
                url  : "#"
            }
        ],
        favouritesPage : [
            {
                title : "Главная",
                url  : "#"
            },
            {
                title : "ИЗБРАННОЕ",
                url  : "#"
            }
        ],
        productsPage : [
            {
                title : "Главная",
                url  : "#"
            },
            {
                title : "ПЕРВЫЕ БЛЮДА         СУП ГОРОХОВЫЙ С КОПЧЕНОСТЯМИ DELILABS",
                url  : "#"
            },
            {
                title : "СУП ГОРОХОВЫЙ С КОПЧЕНОСТЯМИ DELILABS",
                url  : "#"
            }
        ],
        backpackPage: [
            {
                title : "Главная",
                url  : "#"
            },
            {
                title : "МОЙ РЮКЗАК",
                url  : "#"
            }
        ],
        cabinetPage: [
            {
                title : "Главная",
                url  : "#"
            },
            {
                title : "МОЙ КАБИНЕТ",
                url  : "#"
            }
        ],
        contentPage : [
            {
                title : "Главная",
                url  : "#"
            },
            {
                title : " КОНТЕНТНАЯ СТРАНИЦА",
                url  : "#"
            }
        ]
    },
    
    pageTitle: 'marmelad'
};
  
