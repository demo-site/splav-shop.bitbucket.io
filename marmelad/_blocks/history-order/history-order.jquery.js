$('.history-order__short-info').on('click tap', function(){
    var $this = $(this),
        $parent = $this.parent(),
        $content = $this.next();


    $this.toggleClass('history-order__short-info--active')
    $parent.toggleClass('history-order__item--active')   
    $content.slideToggle(300)

});