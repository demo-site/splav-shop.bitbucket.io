$('.b-amount .b-amount__minus').on('click tap', function (event) {
    event.preventDefault();
    var $this = $(this),
        input = $this.parent('.b-amount__controls').find('input'),
        value = parseInt(input.val());

    if (value > 1) {
        input.val(value -= 1);
    }
});

$('.b-amount .b-amount__plus').on('click tap', function (event) {
    event.preventDefault();
    var $this = $(this),
        input = $this.parent('.b-amount__controls').find('input'),
        value = parseInt(input.val());

    input.val(value += 1);
});

$('.b-amount input').keyup(function (e) {
    if (this.value == "0" || this.value == "") {
        this.value = "1";
    } else {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    }
});