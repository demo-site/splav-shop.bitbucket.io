$('.content-slider__items').addClass('owl-carousel').owlCarousel({
    loop:true,
    margin:30,
    nav:false,
    autoplay:true,
    autoplayHoverPause: true,
    smartSpeed: 800,
    responsive:{
        0:{
            items:1
        },
        480:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})