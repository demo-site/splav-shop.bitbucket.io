$('.b-products__wrap-slider').addClass('owl-carousel').owlCarousel({
    items: 1,
    loop: true,
    margin: 0,
    mouseDrag: false,
    nav: true,
    navText: ['', ''],
    dots: true
    // autoplayHoverPause: true,
    // animateOut: 'slideOutUp',
    // animateIn: 'slideInUp'
});
$('.b-products__description-title').addClass('tabs-title-active');
$('.b-products__comments-title').on('click', function(){
    $(this).closest('.b-products__tab-title').find('.b-products__comments-title').addClass('tabs-title-active');
    $(this).closest('.b-products__tab-title').find('.b-products__description-title').removeClass('tabs-title-active');
    $(this).closest('.b-products__tabs').find('.b-products__comments').css('display', 'block');
    $(this).closest('.b-products__tabs').find('.b-products__description').css('display', 'none');
});
$('.b-products__description-title').on('click', function(){
    $(this).closest('.b-products__tab-title').find('.b-products__description-title').addClass('tabs-title-active');
    $(this).closest('.b-products__tab-title').find('.b-products__comments-title').removeClass('tabs-title-active');
    $(this).closest('.b-products__tabs').find('.b-products__comments').css('display', 'none');
    $(this).closest('.b-products__tabs').find('.b-products__description').css('display', 'block');
});
lightbox.option({
    'resizeDuration': 200,
    'wrapAround': true
});
