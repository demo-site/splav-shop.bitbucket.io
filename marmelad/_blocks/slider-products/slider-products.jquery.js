$('.slider-products__items').addClass('owl-carousel').owlCarousel({
    items: 4,
    loop: true,
    margin: 0,
    mouseDrag: false,
    nav: true,
    smartSpeed: 800,
    navText: ['', ''],
    dots: false,
    responsive:{
        320:{
            items: 1,
            center: true
        },
        620:{
            items: 2
        },
        900:{
            items: 3
        },
        1280:{
            items: 4
        }
    }
});