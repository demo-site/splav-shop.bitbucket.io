$(window).scroll(function() {
 
    if($(this).scrollTop() >= 800) {
        $('.page-scroller').fadeIn();
    }
    else {
        $('.page-scroller').fadeOut();
    }
     
});
     
$('.page-scroller').click(function() {

    $('body,html').animate({scrollTop:0},800);

});
