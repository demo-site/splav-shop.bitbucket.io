$('.our-brands__items').addClass('owl-carousel').owlCarousel({
    items: 7,
    loop: true,
    margin: 50,
    mouseDrag: false,
    nav: true,
    smartSpeed: 800,
    navText: ['', ''],
    dots: false,
    responsive:{
        320:{
            items: 2
        },
        481:{
            items: 3
        },
        640:{
            items: 4
        },
        770:{
            items: 5
        },
        960:{
            items: 7
        }
    }
});