$('.b-top-slider__items').owlCarousel({
    items: 1,
    loop: true,
    margin: 0,
    mouseDrag: false,
    nav: true,
    navText: ['', ''],
    dots: true
});